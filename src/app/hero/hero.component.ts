import {Component, OnInit} from '@angular/core';
import {HeroService} from '../service/hero.service';
import {Hero} from '../model/hero';
import {Router} from "@angular/router";


@Component({
  selector: 'toh-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {

  selectedHero: Hero;
  heroes: Hero[];

  constructor(private service: HeroService, private router: Router) {
  }

  ngOnInit(): void {
    this.service.getHeroes().then(heroes => {
      this.heroes = heroes;
    });
  }

  onSelect(hero: Hero) {
    this.selectedHero = hero;
  }

  goToDetail(): void {
    this.router.navigate(['/detail', this.selectedHero.id]);
  }

  add(heroName: string) {
    this.service.create(heroName)
      .then((hero) => {
        this.heroes.push(hero);
        this.selectedHero = null;
      });
  }

  remove(heroId: Hero) {
    this.service.remove(heroId)
      .then(() => {
        this.heroes = this.heroes.filter(h => h !== heroId);
        if (this.selectedHero ===  heroId) {
          this.selectedHero = null;
        }
      });
  }
}

