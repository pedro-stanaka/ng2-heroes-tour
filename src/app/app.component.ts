import {Component} from "@angular/core";
import {HeroService} from "./service/hero.service";


@Component({
  selector: 'toh-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Tour of heroes';
}

